package net.sourceforge.simcpux;

import android.app.Application;

import com.library.sharehelper.WXShareHelper;


/**
 * Wubj 创建于 2017/7/13.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        WXShareHelper.getInstance().init(this,Constants.APP_ID);
    }

}
