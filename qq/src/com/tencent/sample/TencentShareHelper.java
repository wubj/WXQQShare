package com.tencent.sample;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.tencent.connect.share.QQShare;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;

/**
 * Wubj 创建于 2017/7/14.
 */
public class TencentShareHelper {

    private static TencentShareHelper sTencentShareHelper;
    private Tencent mTencent;

    private static final int THUMB_SIZE = 150;

    public void init(Context mContext, String APP_ID) {
        mTencent = Tencent.createInstance(APP_ID, mContext);
    }

    public static synchronized TencentShareHelper getInstance() {
        if (sTencentShareHelper == null) {
            sTencentShareHelper = new TencentShareHelper();
        }
        return sTencentShareHelper;
    }

    public Tencent getTencent() {
        return mTencent;
    }

    /**
     * QQ 默认分享
     */
    public void shareDefaultWithImage(Activity activity, String title, String targetUrl, String summary, String
            imageUrl, IUiListener qqShareListener) {
        Bundle params = new Bundle();
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, targetUrl);
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary);
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_DEFAULT);
        mTencent.shareToQQ(activity, params, qqShareListener);
    }

    public void shareImage(Activity activity, String imageUrl, IUiListener qqShareListener) {
        Bundle params = new Bundle();
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, imageUrl);
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_IMAGE);
        mTencent.shareToQQ(activity, params, qqShareListener);
    }

    public void shareAudio(Activity activity, String title, String targetUrl, String summary, String
            imageUrl, String audioUrl, IUiListener qqShareListener) {
        Bundle params = new Bundle();
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, targetUrl);
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, summary);
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, imageUrl);
        params.putString(QQShare.SHARE_TO_QQ_AUDIO_URL, audioUrl);
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_AUDIO);
        mTencent.shareToQQ(activity, params, qqShareListener);
    }

    public boolean isSessionValid() {
        return mTencent.isSessionValid();
    }

    public void TencentLogin(Activity activity, IUiListener loginListener) {
        if (!isSessionValid()) {
            mTencent.login(activity, "all", loginListener);
        }
    }

    public void TencentLogout(Context mContext) {
        mTencent.logout(mContext);
    }

    public void TencentLogin(Fragment fragment, IUiListener loginListener) {
        mTencent.login(fragment, "all", loginListener);
    }

    public void TencentLoginServerSide(Activity activity, IUiListener loginListener) {
        TencentLogout(activity);
        mTencent.loginServerSide(activity, "all", loginListener);
    }
}
