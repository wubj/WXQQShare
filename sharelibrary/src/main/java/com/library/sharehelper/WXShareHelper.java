package com.library.sharehelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXMusicObject;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.modelmsg.WXVideoObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * Wubj 创建于 2017/7/13.
 */
public class WXShareHelper {

    private static WXShareHelper sWXShareHelper;
    private IWXAPI api;

    private static final int THUMB_SIZE = 150;

    public void init(Context mContext, String APP_ID) {
        api = WXAPIFactory.createWXAPI(mContext, APP_ID);
        api.registerApp(APP_ID);
    }

    public static synchronized WXShareHelper getInstance() {
        if (sWXShareHelper == null) {
            sWXShareHelper = new WXShareHelper();
        }
        return sWXShareHelper;
    }

    public IWXAPI getApi() {
        return api;
    }

    public void shareText(String text, int type) {
        //初始化WXTextObject对象,填写分享的文本内容
        WXTextObject textObj = new WXTextObject();
        textObj.text = text;

        //用WXTextObject对象初始化一个WXMediaMessage对象
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = textObj;
        msg.description = text;

        //构建一个req
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("text"); // transaction字段用于唯一标识一个请求
        req.message = msg;
        req.scene = type;

        // 调用api接口发送数据到微信
        api.sendReq(req);
    }

    public void shareImageBitmap(Bitmap bitmap, int type) {
        WXImageObject imgObj = new WXImageObject(bitmap);

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = imgObj;

        Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, THUMB_SIZE, THUMB_SIZE, true);
        bitmap.recycle();
        msg.thumbData = WxShareUtils.bmpToByteArray(thumbBmp, true);  // ��������ͼ

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("img");
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }

    public void shareImagePath(String imgPath, int type) {
        WXImageObject imgObj = new WXImageObject();
        imgObj.setImagePath(imgPath);

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = imgObj;

        Bitmap bmp = BitmapFactory.decodeFile(imgPath);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
        bmp.recycle();
        msg.thumbData = WxShareUtils.bmpToByteArray(thumbBmp, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("img");
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }

    public void shareMusic(String url, String title, String desc, Bitmap thumbData, int type) {
        WXMusicObject music = new WXMusicObject();
        music.musicUrl = url;

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = music;
        msg.title = title;
        msg.description = desc;

//        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.send_music_thumb);
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
//        bmp.recycle();
        if (thumbData != null) {
            msg.thumbData = WxShareUtils.bmpToByteArray(thumbData, true);
        }

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("music");
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }

    public void shareVideo(String url, String title, String desc, Bitmap thumbData, int type) {
        WXVideoObject video = new WXVideoObject();
        video.videoUrl = url;

        WXMediaMessage msg = new WXMediaMessage(video);
        msg.title = title;
        msg.description = desc;
        if (thumbData != null) {
            msg.thumbData = WxShareUtils.bmpToByteArray(thumbData, true);
        }

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("video");
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }

    public void shareWebUrl(String url, String title, String desc, Bitmap thumbData, int type) {
        WXWebpageObject webPage = new WXWebpageObject();
        webPage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webPage);
        msg.title = title;
        msg.description = desc;
        if (thumbData != null) {
            msg.thumbData = WxShareUtils.bmpToByteArray(thumbData, true);
        }
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }

    public void sendLogin(Context mContext) {
        if (api != null && api.isWXAppInstalled()) {
            SendAuth.Req req = new SendAuth.Req();
            req.scope = "snsapi_userinfo";
            req.state = "wechat_sdk_demo_test";
            api.sendReq(req);
        } else {
            Toast.makeText(mContext, "用户未安装微信", Toast.LENGTH_SHORT).show();
        }
    }

    public String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }
}
